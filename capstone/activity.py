from abc import ABC, abstractmethod
# 1.
class Person(ABC):
    @abstractmethod
    def getFullName(self):
        pass;
    @abstractmethod
    def addRequest(self):
        pass;
    @abstractmethod
    def checkRequest(self):
        pass;
    @abstractmethod
    def addUser(self):
        pass;
# 2.
class Employee (Person):
    def __init__ (self, firstName, lastName, email, department):
        super().__init__();
        self._firstName = firstName;
        self._lastName = lastName;
        self._email = email;
        self._department = department;
    # SETTER
    def set_firstName(self, firstName):
        self._firstName = firstName;

    def set_lastName(self, lastName):        
        self._lastName = lastName;

    def set_email(self, email):
        self._email = email;

    def set_department(self, department):
        self._department = department;

    # GETTER
    def get_firstName(self):
        return self._firstName; 

    def get_lastName(self):        
        return self._lastName; 

    def get_email(self):
        return self._email;

    def get_department(self):
        return self._department;

    # ABSTACT 
    def getFullName(self):
        return self._firstName + " " + self._lastName; 

    def addRequest(self):
        return "Request has been added"; 

    def checkRequest(self):
        pass;

    def addUser(self):
        pass;

    # METHOD
    def login(self):
        return f"{self._email} has logged in"; 

    def logout(self):
        return f"{self._email} has logged out";

# 3.
class TeamLead (Employee):
    def __init__ (self, firstName, lastName, email, department):
        super().__init__(firstName, lastName, email, department);
        self._members = [];

    # SETTER
    def addMember(self, member):
        self._members.append(member);


     # GETTER
    def get_members(self):
        return self._members;
          
    # ABSTACT 
    def getFullName(self):
        return self._firstName + " " + self._lastName;

    def addRequest(self):
        pass

    def checkRequest(self):
        return "Request has been checked";

    def addUser(self):
        pass;


# 4.
class Admin (Employee):
    def __init__ (self, firstName, lastName, email, department):
        super().__init__(firstName, lastName, email, department);
          
    # ABSTACT 
    def getFullName(self):
        return self._firstName + " " + self._lastName;

    def addRequest(self):
        pass

    def checkRequest(self):
        pass        

    def addUser(self):
        return f"User has been added";

# 5.
class Request ():
    def __init__ (self, name, requester, dateRequested):
        self._name = name;
        self._requester = requester;
        self._dateRequested = dateRequested;
        self._status = "open";

    # Methods
    def set_status(self, status):
        self._status = status;

    def updateRequest(self):
        return f"Request {self._name} has been updated";

    def closeRequest(self):
        return f"Request {self._name} has been closed";

    def cancelRequest(self):
        return f"Request {self._name} has been cancel";



#Test Case
employee1 = Employee ("John", "Doe", "djohn@mail.com", "Marketing");
employee2 = Employee ("Jane", "Smith", "sjane@mail.com", "Marketing");
employee3 = Employee ("Robert", "Patterson", "probert@mail.com", "Sales");
employee4 = Employee ("Brandon", "Smith", "sbrandon@mail.com", "Sales");
admin1 = Admin ("Monika", "Justin", "jmonika@mail.com", "Marketing");
teamLead1 = TeamLead("Micheal", "Specter", "smicheal@mail.com", "Sales");
req1 = Request ("New hire orientation", teamLead1, "27-Jul-2021");
req2 = Request ("Laptop Repair", employee1, "1-Jul-2021");

assert employee1.getFullName() == "John Doe", "Full name should be John Doe";
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin";
assert employee1.getFullName() == "John Doe", "Full name should be John Doe";
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3);
teamLead1.addMember(employee4);
for indiv_emp in teamLead1.get_members():
    print (indiv_emp.getFullName());

assert admin1.addUser() == "User has been added";

req2.set_status("closed");
print(req2.closeRequest());