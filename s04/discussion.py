class SampleClass():
    def __init__(self, year):
        self.year = year

    def show_year(self):
        print(f"The year is: {self.year}")

# Create an instance
myObj = SampleClass(2023)

print(myObj.year)
myObj.show_year()

# [SECTION] Fundamentals of OOP
# There are four main fundamental principles of OOP
# Encapsulation
# Inheritance
# Polymorphism
# Abstraction

# [SECTION] Encapsulation
# In encapsulation, the attributes of a class will be hidden from other classes
# To achieve encapsulation - 
# 1. Declare the attributes of a class
# 1. Provide getter and setter methods to modify and view the attribute values

class Person():
    def __init__(self): 
        # protected attribute _name
        self._name = "John Doe"
        self._age = 0

    def set_name(self, name):
        self._name = name

    def get_name(self):
        print(f"Name of person: {self._name}")

    def set_age(self, age):
        self._age = age

    def get_age(self):
        print(f"Age of person: {self._age}")

p1 = Person()
p1.get_name()
p1.set_name("Bob Doe")
p1.get_name()
print(p1._name)

# Test Cases:
p1.set_age(38)
p1.get_age()


# [SECTION] Inheritance
# To create an inherited class, add the parent class
# Syntax: class ChildClassName(ParentClassName)
class Employee(Person):
    def __init__(self, employeeId):
        # super() can be used to invoke the immediate parent class constructor
        super().__init__()
        # attributes unique to the Employee class
        self._employeeId = employeeId

    # methods of the Employee class
    # getter method
    def get_employeeId(self):
        print(f"The Employee ID is {self._employeeId}")

    # setter method
    def set_employeeId(self, employeeId):
        self._employeeId = employeeId

    def get_details(self):
        print(f"{self._employeeId} belongs to {self._name}")

emp1 = Employee("Emp-001")
emp1.get_details()
emp1.set_name("Jane Doe")
emp1.set_age(40)
emp1.get_age()
emp1.get_details()

# Mini exercise
# 1. Create a new class called Student that inherits Person with the additional attributes and methods
# attributes: Student No, Course, Year Level
# methods: 
#   get_detail: prints the output "<Student name> is currently in year <year level> taking up <Course>"

class Student(Person):
    def __init__(self, studentNo, course, year_level):
        super().__init__()

        self._studentNo = studentNo
        self._course = course
        self._year_level = year_level

    # getters
    def get_studentNo(self):
        print(f"Student number of student is {self._studentNo}")

    def get_course(self):
        print(f"Course of Student is {self._course}")

    def get_year_level(self):
        print(f"The year level of the student is {self._year_level}")

    # setters
    def set_studentNo(self, studentNo):
        self._studentNo = studentNo

    def set_course(self, course):
        self._course = course

    def set_year_level(self, year_level):
        self._year_level = year_level

    def get_details(self):
        print(f"{self._name} is currently in year {self._year_level} taking up {self._course}")

# Test cases:
student1 = Student("stdt-001", "Computer Science", 1)
student1.set_name("Brandon Smith")
student1.set_age(18)
student1.get_details()