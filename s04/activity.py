from abc import ABC, abstractmethod

class Animal(ABC):
    @abstractmethod
    def eat(self,food):
        pass;
    @abstractmethod
    def make_sound(self):
        pass;

class Cat(Animal):
    def __init__ (self, name, breed, age):
        super().__init__()
        self._name = name;
        self._breed = breed;
        self._age = age;

    def eat(self,food):
        print (f"Serve me {food}");

    def make_sound(self):
        print (f"Miaow! Nyaw! Nyaaaa!");

    def call(self):
        print(f"{self._name}, come on!")


    # Setter
    def set_name(self,name):
        self._name = name;

    def set_breed(self,breed):       
        self._breed = breed;

    def set_age(self,age):        
        self._age = age;

    # Getter
    def get_name(self):
        return self._name;

    def get_breed(self):       
        return self._breed;

    def get_age(self):        
        return self._age;


class Dog(Animal):
    def __init__ (self, name, breed, age):
        super().__init__()
        self._name = name;
        self._breed = breed;
        self._age = age;

    def eat(self,food):
        print (f"Eaten {food}");

    def make_sound(self):
        print (f"Bark! Woof! Arf!");

    def call(self):
        print(f"Here {self._name}!")


    # Setter
    def set_name(self,name):
        self._name = name;

    def set_breed(self,breed):       
        self._breed = breed;

    def set_age(self,age):        
        self._age = age;

    # Getter
    def get_name(self):
        return self._name;

    def get_breed(self):       
        return self._breed;

    def get_age(self):        
        return self._age;



dog1 = Dog ("Isis", "German Shepard", 3);

cat1 = Cat ("Puss", "Persian Cat", 2);

dog1.eat("Steak");
dog1.make_sound();
dog1.call();

cat1.eat("Tuna");
cat1.make_sound();
cat1.call();

