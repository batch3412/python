# Python allows for user input
# The input() method allows users to give inputs to the program
# username= input("Please enter your name: ")
# print(f"Hello {username}! Welcome to Python Short Course!")

# Input automatically assigns input as a string. To solve this, we can convert the data type using int()
# num1 = int(input("Enter 1st number:"))
# num2 = int(input("Enter 2nd number:"))
# print(f"The sum of num1 and num2 is {num1 + num2}")

# [SECTION] If-else statements
# If-else statements are used to choose between two or more code blocks depending on the condition

# Declare a variable to use for the conditional statement
# test_num = 75

# if test_num >= 60:
# 	print("Test passed")
# else:
# 	print("Test failed")

# Note: Indentations are important in Python as it uses indentations to distinguish parts of the code

# test_num2 = int(input("Please enter the 2nd test number:"))
# if test_num2 > 0:
# 	print("The number is positive")
# elif test_num2 == 0:
# 	print("The number is zero")
# else:
# 	print("The number is negative")

# Mini Exercise 1:
# Create an if-else statement that determines if a number is divisible by 3, 5, or both. 
# If the number is divisible by 3, print "The number is divisible by 3"
# If the number is divisible by 5, print "The number is divisible by 5"
# If the number is divisible by 3 and 5, print "The number is divisible by both 3 and 5"
# If the number is not divisible by any, print "The number is not divisible by 3 nor 5"

# test_div_num = int(input("Please eneter a number to test: "))
# if test_div_num % 3 == 0 and test_div_num % 5 == 0:
# 	print(f"{test_div_num} is divisible by both 3 and 5")
# elif test_div_num % 3 == 0:
# 	print(f"{test_div_num} is divisible by 3") 
# elif test_div_num % 5 == 0:
# 	print(f"{test_div_num} is divisible by 5")
# else:
# 	print(f"{test_div_num} is not divisible by both 3 nor 5")


# [SECTION] Loops

# Python has loops that can repeat blocks of code:
# While loops are used to execute a set of statements as long as the condition is true
i = 1
while i <= 5:
	print(f"Current count: {i}")
	i += 1

# For loops are used for iterating over a sequence
fruits = ["apple", "banana", "cherry"]
for indiv_fruit in fruits:
	print(indiv_fruit)

# range() method
# The range() method defaults to 0 as a starting value
# range(stop), range(start, stop), range(start, stop, step/increment)
# will print values from 0-5
for x in range(6):
	print(f"The current value is {x}")

# Prints values from 6 - 9
# Note: range() always prints to n-1
for x in range(6, 10):
	print(f"The current value is {x}")

# A third argument cam be added to specify the increment of the loop
for x in range(6, 20, 2):
	print(f"The current value is {x}")

# Mini Exercise 2:
# Create a loop to count and display the number of even numbers between 1 and 20.
# Print "The number of even numbers between 1 and 20 is: <total number of even numbers>"

count = 0 # counter

for num in range(1, 21): # Loop through number 1-20
	if num % 2 == 0: # Checks if the number is even
		count += 1 # Increment the counter

print("The number of even numbers between 1 and 20 is:", count)

# Mini Exercise 3:
# Write a Python program that takes an integer input from the user and displays the multiplication table for that number, from 1 to 10.

# For example:
    # 5 x 1 = 5
    # 5 x 2 = 10
    # 5 x 3 = 15
    # 5 x 4 = 20
    # 5 x 5 = 25
    # 5 x 6 = 30
    # 5 x 7 = 35
    # 5 x 8 = 40
    # 5 x 9 = 45
    # 5 x 10 = 50

# num = int(input("Enter any number:"))

# for i in range(1, 11):
# 	result = num * i
# 	print(num, "x", i, "=", result)

# [SECTION] Break Statement
# The break statement is used to stop the loop
j = 1
while j < 6:
	print(j)
	if j == 3:
		break
	j += 1
# When j reaches 3, the loop ends and the next iterations are not run


# [SECTION] Continue Statement
# The continue statement returns the control to the beginning of the while loop and continue with the next iteration
k = 1
while k < 6:
	k += 1
	if k == 3:
		continue
	print(k)