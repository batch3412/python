name = "Naruto"
age = 18
occupation = "ninja"
movie = "Naruto Shippuden"
rating = 100

print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating}%")

num1 = 15
num2 = 10
num3 = 20
print(int(num1) * int(num2))
print(num1 < num3)
print(int(num3) + int(num2))