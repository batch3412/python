class Camper():
	def __init__(self, name, batch, course_type):
		self.name = name
		self.batch = batch
		self.course_type = course_type

	def career_track(self, course_type):
		print(f"Currently enrolled in the {self.course_type} program")

	def info(self, name, batch):
		print(f"My name is {self.name} of batch {self.batch}.")

zuitt_camper = {
	"name": "Glenn",
	"batch": 341,
	"course_type": "python short course"
}
print(f"Campers Name: {zuitt_camper['name']}")
print(f"Campers Batch: {zuitt_camper['batch']}")
print(f"Campers Course: {zuitt_camper['course_type']}")

new_camper = Camper("Glenn", "341", "python short course")

new_camper.info("Glenn", "341")
new_camper.career_track("python short course")

